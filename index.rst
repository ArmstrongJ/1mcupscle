
.. A Short Intro to Approximatrix slides file, created by
   hieroglyph-quickstart on Mon Aug 25 12:13:21 2014.

==============================
A Short Intro to 
==============================

.. image:: _static/logo_big_words.png

Jeffrey Armstrong

August 27th, 2014

http://approximatrix.com/

My Background
=============

 * Aerospace Engineer
 * Love programming
 * Independent

History
=======

Founded in 2006...

.. figure:: _static/draco.png
   :figwidth: 100%
   :align: center

to market a statistics package

Lessons Learned
===============

Draco was a failure in every way
 * Scope was unachievable
 * Exceptionally slow updates
 * No market research

Ideals don't equal profit

Simply Fortran
==============

Released July 2010

.. figure:: _static/sfortran.png
   :figwidth: 100%
   :align: center

Filled Very Specific Niche

Inexpensive!

Simply Fortran
==============

Slow Start
 * Single-digit sales per month for first 6 months
 * Word-of-mouth advertising
 
Rapid Release Schedule
 * One release per month
 
Responsive
 * Features generally dictated by customer requests

Growth
======

====================   =========
Package Manager:       |sfpm|
Corporate Customers:   |sfls|
Additional Products:   |seditor|
====================   =========

.. |sfpm| image:: _static/sfpm.png
   :scale: 60 %
   
.. |sfls| image:: _static/sfls.png
   :scale: 60 %
   
.. |seditor| image:: _static/seditor.png
   :align: middle
   
Issues
======

Marketing
 * How does one contact such a diverse group?

Pricing
 * Four drastic price changes

Summary
=======

A success story?

Contact:
 * Email: jeff@approximatrix.com
 * Twitter: @fortranjeff or @SimplyFortran
 * http://approximatrix.com/
 * http://simplyfortran.com/
